TAME-Diff

Time-aligned Metadata Differencing Tool
=======================================

v0.2 2014-06-16

License: LGPL v3

Andras Horti <andras.horti@joanneum.at>
Werner Bailer <werner.bailer@joanneum.at>

JOANNEUM RESEARCH - DIGITAL, Graz, Austria
http://www.joanneum.at/digital

ABOUT

TAME-Diff is a tool to determine differences between metadata documents, 
focusing on time-aligned metadata of audiovisual content. The input to the
tool are two metadata documents (one of them being the reference document,
i.e., often the ground truth). In case the documents contain multiple metadata
time lines, the time line to be compared can be specified (using XPath 
expressions). The tool then performs longest common subsequence (LCS) alignment
of the segments in the time line, allowing for insertions and deletions. 

TAME-Diff currently supports the MPEG-7 Audiovisual Description Profile (AVDP), 
see http://mpeg.chiariglione.org/standards/mpeg-7/profiles

The output is represented in an extended version of Delta Update Language 
(DUL), see http://diffxml.sourceforge.net/dul/dul.html

USAGE

-O outputfile referencefile otherfile


----------------------------------------

The research leading to these results has received funding from the European 
Union's Seventh Framework Programme (FP7/2007-2013) under grant agreement no.
287532, TOSCA-MP - Task-oriented search and content annotation for media
production (http://www.tosca-mp.eu).