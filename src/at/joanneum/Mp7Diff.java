/*

  TAME-Diff 
  
  Developed with the TOSCA-MP project (http://www.tosca-mp.eu)
    
  2014 JOANNEUM RESEARCH Forschungsgesellschaft mbH, Graz, Austria
  http://www.joanneum.at/en/digital/
  
  
  ----------------------------------------------------------------------------
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


 */

package at.joanneum;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import at.joanneum.tools.JrsXmlTools;

/***
 * 
 * @author hoa
 * 
 */

// TODO
// - backtrack all in 2 directions
// - find the best overlap
// - create split,merge report for the diff

class nodeData {
  public nodeData(String nodeID, long StartTime, long EndTime, Node domNode) {
    mNodeID = nodeID;
    mStartTime = StartTime;
    mEndTime = EndTime;
    mOrigDomNode = domNode;
  }

  public String mNodeID;
  public long mStartTime;
  public long mEndTime;
  public Node mOrigDomNode;
};

public class Mp7Diff {

  private final static String Text_insert = "insert";
  private final static String Text_delete = "delete";
  private final static String Text_parent = "parent";
  private final static String Text_childno = "childno";
  private final static String Text_nodetype = "nodetype";
  private final static String Text_name = "name";
  private final static String Text_node = "node";
  private final static String Text_merge = "merge";
  private final static String Text_split = "split";
  private final static String Text_source = "source";
  private final static String Text_target = "target";
  private final static String Text_update = "update";
  private final static String Text_charpos = "charpos";
  private final static String Text_length = "length";
  private final static String mMtPattern = "\\D*(\\d+)N(\\d+)F";
  private final static XPath mXpath = XPathFactory.newInstance().newXPath();
  private static XPathExpression mXpExprTimePoint = null;
  private static XPathExpression mXpExprDuration = null;

  private static double mEpsilon = 0.5; // part of segment
  private static double mEpsilonMin = 0.5; // in s
  private static double mEpsilonMax = 7; // in s
  private static boolean mCompareGE = true;
  private static String mOutputFileName = "Mpeg7Diff.xml";
  private static String mParamBaseFileName = null;
  private static Vector<String> mParamToDiffFileName = null;
  private static boolean mVerbose = false;

  private String mBaseFileName = null;
  private String mToDiffFileName = null;
  private HashSet<String>[][] mBckRes = null;
  private static Element mDulSetElement = null;
  private Element mDulElement = null;
  private Node mLastBaseNode = null;
  private DocumentBuilder mDocBuilder = null;
  private int mIsMatch[][];
  private int mWasChecked[][];
  private HashMap<Node, Integer> mParentRelChildCount = new HashMap<Node, Integer>();

  final static String mExprTimePoint = "MediaTime/MediaRelIncrTimePoint/text()";
  final static String mExprDuration = "MediaTime/MediaIncrDuration/text()";
  final static String mExprTimePointNode = "MediaTime/MediaRelIncrTimePoint";
  final static String mExprDurationNode = "MediaTime/MediaIncrDuration";
  final private static String mXPathAudiosVisual = "/Mpeg7/Description/MultimediaContent/AudioVisual/TemporalDecomposition/AudioVisualSegment";
  final private static String mXPathAudiosVisualId = "/Mpeg7/Description/MultimediaContent/AudioVisual/TemporalDecomposition/AudioVisualSegment/@id";
  final private static String mXPathAudiosVisualTime = "/Mpeg7/Description/MultimediaContent/AudioVisual/TemporalDecomposition/AudioVisualSegment/MediaTime/MediaRelIncrTimePoint/text()";
  final private static String mXPathAudiosVisualDuration = "/Mpeg7/Description/MultimediaContent/AudioVisual/TemporalDecomposition/AudioVisualSegment/MediaTime/MediaIncrDuration/text()";
  final private static String mXpathMediaTimeUnit = "/Mpeg7/Description/MultimediaContent/AudioVisual[@mediaTimeUnit]/@mediaTimeUnit";

  /***
   * Protected constructor for the class. The static functions mp7Diff() should
   * be used.
   * 
   * @param baseFileName
   *          - reference file name
   * @param toDiffFileName
   *          - benchmark file name
   * @param dulElement
   *          - the DUL node where the data must be appended to
   * @param docBuilder
   *          - the actual document builder.
   */
  protected Mp7Diff(String baseFileName, String toDiffFileName, Element dulElement, DocumentBuilder docBuilder) {
    mBaseFileName = baseFileName;
    mToDiffFileName = toDiffFileName;
    mDulElement = dulElement;
    mDocBuilder = docBuilder;
    mLastBaseNode = null;
    mIsMatch = null;
    mWasChecked = null;
    mParentRelChildCount = new HashMap<Node, Integer>();
  }

  /***
   * Read the base time unit defined by the xpExpr into a double value
   * 
   * @param xpExpr
   *          - xpath expression of the base time unit node
   * @param xmlDoc
   *          - the xml DOM of the document
   * @return - the double value of the base time unit
   */
  protected static double getBaseTimeUnit(XPathExpression xpExpr, Document xmlDoc) {
    double retval = 1.0 / 25; // default 25 fps = 40 ms
    try {
      String mtUnitBase = xpExpr.evaluate(xmlDoc);
      if (mtUnitBase != null) {
        // double mtBaseTimeUnit = 1.0 / 25; // default 25 fps = 40 ms
        int nnI, ndI;
        Pattern pt = Pattern.compile(mMtPattern);
        Matcher m = pt.matcher(mtUnitBase);
        if (m.find()) {
          String nn = m.group(1);
          String nd = m.group(2);
          if (nn != null && nd != null) {
            nnI = Integer.parseInt(nn);
            ndI = Integer.parseInt(nd);
            if (ndI > 0) {
              retval = ((double) nnI) / ndI;
            }
          }
        }
      }
    } catch (XPathExpressionException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return (retval);
  }

  /***
   * Do the backtracking for the LCS Length matrix created by the lcsLength()
   * function
   * 
   * @param cVal
   *          - the LCS Length matrix
   * @param mtBaseTimeUnit
   *          - time unit of the reference media
   * @param mtToDiffTimeUnit
   *          - time unit of the benchmark media
   * @param epsilon
   *          - the allowed deviation of between boundaries (relative value,part
   *          of the segment length)
   * @param baseNodeDataArr
   *          - data vector with the values of the reference media
   * @param diffNodeDataArr
   *          - data vector with the values of the benchmark media
   * @param i
   *          - index of the baseNodeDataArr
   * @param k
   *          - index of the diffNodeDataArr
   * @param showX
   *          - if true then collect by reference data otherwise collect by
   *          benchmark data
   * @param compareGE
   *          - if true then backtrack all directions otherwise backtrack only
   *          one direction
   * @return - result of backtracking, the longest matching chains
   * @throws XPathExpressionException
   */
  protected HashSet<String> backTrackAll(int[][] cVal, double mtBaseTimeUnit, double mtToDiffTimeUnit, double epsilon,
      nodeData[] baseNodeDataArr, nodeData[] diffNodeDataArr, int i, int k, boolean showX, boolean compareGE)
      throws XPathExpressionException {
    HashSet<String> retval = null;
    if (i < 0 || k < 0) {
      retval = new HashSet<String>();
      retval.add(new String());
    } else {
      if (mBckRes[i][k] != null) {
        return (mBckRes[i][k]);
      }
      boolean ndsEqual = matchNodes(baseNodeDataArr[i], diffNodeDataArr[k], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon);
      nodeData dispArrElem = showX ? baseNodeDataArr[i] : diffNodeDataArr[k];
      if (ndsEqual) {
        HashSet<String> zVal = null;
        zVal = backTrackAll(cVal, mtBaseTimeUnit, mtToDiffTimeUnit, epsilon, baseNodeDataArr, diffNodeDataArr, i - 1, k - 1, showX,
            compareGE);
        int zSize = zVal.size();
        String[] zvArr = zVal.toArray(new String[zSize]);
        String eStr;
        for (int o = 0; o < zSize; o++) {
          eStr = zvArr[o];
          zVal.remove(eStr);
          eStr = eStr + "~[" + dispArrElem.mStartTime + "-" + dispArrElem.mEndTime + "]";
          zVal.add(eStr);
        }
        retval = zVal;
      } else {
        retval = new HashSet<String>();
        int cvikm1 = 0;
        int cvim1k = 0;
        if (k > 0)
          cvikm1 = cVal[i][k - 1];
        if (i > 0)
          cvim1k = cVal[i - 1][k];
        if (mVerbose)
          System.out.println("backtrackall NE i:" + i + " k:" + k + " bs:" + baseNodeDataArr[i].mStartTime + " ds:"
              + diffNodeDataArr[k].mStartTime);
        if (i >= 0 && k >= 0)
          mWasChecked[i][k] = 1;
        boolean resComp = compareGE ? cvikm1 >= cvim1k : cvikm1 > cvim1k;
        if (resComp) {
          retval = backTrackAll(cVal, mtBaseTimeUnit, mtToDiffTimeUnit, epsilon, baseNodeDataArr, diffNodeDataArr, i, k - 1, showX,
              compareGE);
        }
        if (cvim1k >= cvikm1) {
          HashSet<String> rVal = backTrackAll(cVal, mtBaseTimeUnit, mtToDiffTimeUnit, epsilon, baseNodeDataArr, diffNodeDataArr, i - 1, k,
              showX, compareGE);
          retval.addAll(rVal);
        }
      }
      mBckRes[i][k] = new HashSet<String>(retval);
    }
    return (retval);
  }

  /***
   * Check the position of the 2 nodes.
   * 
   * @param baseNodeData
   *          - data of the reference node
   * @param compNodeData
   *          - data of the benchmark node
   * @param mtBaseTimeUnit
   *          - time unit of the reference media
   * @param mtCompTimeUnit
   *          - time unit of the benchmark media
   * @param epsilon
   *          - the allowed deviation of between boundaries (relative value,part
   *          of the segment length)
   * @return 0 - If compNode outside of baseNode 1 - If compNode inside of
   *         baseNode 2 - If compNode partially in baseNode
   */
  protected static int testNode(nodeData baseNodeData, nodeData compNodeData, double mtBaseTimeUnit, double mtCompTimeUnit, double epsilon) {
    int retval = 0;
    double bStart = baseNodeData.mStartTime * mtBaseTimeUnit;
    double bEnd = baseNodeData.mEndTime * mtBaseTimeUnit;
    double dStart = compNodeData.mStartTime * mtCompTimeUnit;
    double dEnd = compNodeData.mEndTime * mtCompTimeUnit;
    double dDuration = bEnd - bStart;
    double locEpsilon = (dDuration * epsilon);
    locEpsilon = Math.max(locEpsilon, mEpsilonMin);
    locEpsilon = Math.min(locEpsilon, mEpsilonMax);
    if (dStart >= (bStart - locEpsilon / 2) && dEnd <= (bEnd + locEpsilon / 2)) {
      retval = 1;
    } else if (dStart >= (bStart - locEpsilon) && dStart <= bEnd) {
      retval = Math.abs(bEnd - dEnd) < locEpsilon ? 1 : 2;
      if (retval != 1)
        retval = Math.abs(bEnd - dEnd) + Math.abs(bStart - dStart) < (2 * locEpsilon) ? 1 : 2;
    } else if (dEnd <= (bEnd + locEpsilon) && dEnd >= bStart) {
      retval = Math.abs(bStart - dStart) < locEpsilon ? 1 : 2;
      ;
      if (retval != 1)
        retval = Math.abs(bEnd - dEnd) + Math.abs(bStart - dStart) < (2 * locEpsilon) ? 1 : 2;
      ;
    }
    return (retval);
  }

  /***
   * Check if the baseNode and diffNode have matching boundary times according
   * to the allowed epsilon deviation
   * 
   * @param baseNodeData
   *          - data of the reference node
   * @param diffNodeData
   *          - data of the benchmark node
   * @param mtBaseTimeUnit
   *          - time unit of the reference media
   * @param mtToDiffTimeUnit
   *          - time unit of the benchmark media
   * @param epsilon
   *          - the allowed deviation of between boundaries (relative value,part
   *          of the segment length)
   * @return - true if match otherwise false
   */
  protected static boolean matchNodes(nodeData baseNodeData, nodeData diffNodeData, double mtBaseTimeUnit, double mtToDiffTimeUnit,
      double epsilon) {
    boolean retval = false;
    double bStart = baseNodeData.mStartTime * mtBaseTimeUnit;
    double bEnd = baseNodeData.mEndTime * mtBaseTimeUnit;
    double dStart = diffNodeData.mStartTime * mtToDiffTimeUnit;
    double dEnd = diffNodeData.mEndTime * mtToDiffTimeUnit;
    double dDuration = bEnd - bStart;
    double locEpsilon = (dDuration * epsilon);
    locEpsilon = Math.max(locEpsilon, mEpsilonMin);
    locEpsilon = Math.min(locEpsilon, mEpsilonMax);
    if (dStart >= (bStart - locEpsilon / 2) && dEnd <= (bEnd + locEpsilon / 2)) {
      retval = true;
    } else if (dStart <= bStart && dEnd >= bEnd) {
      retval = true;
    } else if (dStart >= (bStart - locEpsilon) && dStart <= bEnd) {
      retval = Math.abs(bEnd - dEnd) < locEpsilon;
      if (!retval)
        retval = Math.abs(bEnd - dEnd) + Math.abs(bStart - dStart) < (2 * locEpsilon);
    } else if (dEnd <= (bEnd + locEpsilon) && dEnd >= bStart) {
      retval = Math.abs(bStart - dStart) < locEpsilon;
      if (!retval)
        retval = Math.abs(bEnd - dEnd) + Math.abs(bStart - dStart) < (2 * locEpsilon);
    } else {
      retval = (Math.abs(bStart - dStart) + Math.abs(bEnd - dEnd)) < locEpsilon;
    }
    return (retval);
  }

  /***
   * Create the LCSLength matrix comparing the segment times of 2 nodes
   * 
   * @param mtBaseTimeUnit
   *          - time unit of the reference media
   * @param mtToDiffTimeUnit
   *          - time unit of the benachmark media
   * @param epsilon
   *          - the allowed deviation of between boundaries (relative value,part
   *          of the segment length)
   * @param baseNodeDataArr
   *          - data array of the reference media
   * @param diffNodeDataArr
   *          - data array of the benchmark media
   * @return
   */
  private int[][] lcsLength(double mtBaseTimeUnit, double mtToDiffTimeUnit, double epsilon, nodeData[] baseNodeDataArr,
      nodeData[] diffNodeDataArr) {
    // int bCount = baseNodes.size();
    // int dCount = diffNodes.size();
    int bCount = baseNodeDataArr.length;
    int dCount = diffNodeDataArr.length;
    int Cval[][] = new int[bCount][dCount];
    mIsMatch = new int[bCount][dCount];
    mWasChecked = new int[bCount][dCount];
    boolean ndsEqual = false;
    nodeData baseNodeData = null;
    nodeData diffNodeData = null;
    for (int i = 0; i < bCount; i++) {
      for (int k = 0; k < dCount; k++) {
        Cval[i][k] = 0;
        mIsMatch[i][k] = 0;
        mWasChecked[i][k] = 0;
      }
    }
    for (int i = 0; i < bCount; i++) {
      for (int k = 0; k < dCount; k++) {
        int ww = i;
        ndsEqual = matchNodes(baseNodeDataArr[i], diffNodeDataArr[k], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon);
        if (ndsEqual) {
          if (i > 0 && k > 0) {
            Cval[i][k] = Cval[i - 1][k - 1] + 1;
          } else {
            Cval[i][k] = 1;
          }
          mIsMatch[i][k] = 1;
        } else {
          int lcsValLeft = 0;
          int lcsValUp = 0;
          if (i > 0) {
            lcsValUp = Cval[i - 1][k];
          }
          if (k > 0) {
            lcsValLeft = Cval[i][k - 1];
          }
          Cval[i][k] = Math.max(lcsValLeft, lcsValUp);
        }
      }
    }
    return (Cval);
  }

  /***
   * Generate the DUL insert statements for node.
   * 
   * @param dulElement
   *          - the parent DUL element
   * @param node
   *          - the source node
   * @param actXpath
   *          - the actual Xpath
   * @param childNoPar
   *          - the actual child no of the node
   * @param parentNode
   *          - the basic node of the actXpath
   */
  protected static void putDulInsert(Element dulElement, Node node, String actXpath, String childNoPar, Node parentNode) {
    addDulInsertElement(dulElement, actXpath, childNoPar, String.valueOf(node.getNodeType()), node.getNodeName(), null);
    String rxPath = JrsXmlTools.getRelXpath(node, parentNode, true);
    String bnXpath = actXpath + "/" + rxPath;
    NodeList chList = node.getChildNodes();
    NamedNodeMap caMap = node.getAttributes();
    int caLen = caMap.getLength();
    for (int ll = 0; ll < caLen; ll++) {
      Node chNode = caMap.item(ll);
      putDulContent(dulElement, chNode, bnXpath, null, node);
    }
    int chLen = chList.getLength();
    int childNo = 1;
    String childNoStr = null;
    for (int ll = 0; ll < chLen; ll++) {
      short chnType = node.getNodeType();
      Node chNode = chList.item(ll);
      if (chnType == Node.ELEMENT_NODE || chnType == Node.TEXT_NODE) {
        childNoStr = String.valueOf(childNo);
        childNo++;
      } else
        childNoStr = null;
      putDulContent(dulElement, chNode, bnXpath, childNoStr, node);
    }
  }

  /***
   * Generate the DUL nodes for the actual node content.
   * 
   * @param dulElement
   *          - the DUL Element to append to
   * @param node
   *          - the node to process
   * @param actXpath
   *          - the actual Xpath
   * @param childNo
   *          - the actual child no of the node
   * @param parentNode
   *          - the basic node of the actXpath
   */
  protected static void putDulContent(Element dulElement, Node node, String actXpath, String childNo, Node parentNode) {
    short chnType = node.getNodeType();
    String chnVal = null;
    String chnName = null;
    switch (chnType) {
      case Node.ATTRIBUTE_NODE:
        chnVal = node.getNodeValue();
        chnName = node.getNodeName();
        break;
      case Node.COMMENT_NODE:
        break;
      case Node.DOCUMENT_NODE:
        break;
      case Node.ELEMENT_NODE:
        putDulInsert(dulElement, node, actXpath, childNo, parentNode);
        break;
      case Node.TEXT_NODE:
        chnVal = node.getNodeValue();
        break;
    }
    if (chnVal != null || chnName != null) {
      addDulInsertElement(dulElement, actXpath, childNo, String.valueOf(chnType), chnName, chnVal);
    }
  }

  /***
   * Create a DUL insert element
   * 
   * @param baseDulElement
   *          - the DUL element to append to
   * @param xpath
   *          - parent xpath
   * @param childNo
   *          - the child no of the node
   * @param nodeType
   *          - the type of the node
   * @param nodeName
   *          - node name
   * @param txtContent
   *          - the text content of the node
   */
  protected static void addDulInsertElement(Node baseDulElement, String xpath, String childNo, String nodeType, String nodeName,
      String txtContent) {
    Element elIns = JrsXmlTools.AddChildElement(baseDulElement, Text_insert);
    JrsXmlTools.AddChildAttribute(elIns, Text_parent, xpath);
    if (childNo != null)
      JrsXmlTools.AddChildAttribute(elIns, Text_childno, childNo);
    JrsXmlTools.AddChildAttribute(elIns, Text_nodetype, nodeType);
    if (nodeName != null)
      JrsXmlTools.AddChildAttribute(elIns, Text_name, nodeName);
    if (txtContent != null)
      elIns.setTextContent(txtContent);
  }

  /***
   * Generate a DUL insert relative to the last base node of the reference media
   * 
   * @param dulElement
   *          - the DUL element to append to
   * @param lastBaseNode
   *          - the last node of the reference media
   * @param node
   *          - actual node of the benchmark media
   */
  protected void putDulInsert(Element dulElement, Node lastBaseNode, Node node) {
    if (lastBaseNode != null) {
      int childCorr = 0;
      if (mParentRelChildCount.containsKey(lastBaseNode.getParentNode())) {
        childCorr = mParentRelChildCount.get(lastBaseNode.getParentNode()).intValue();
      }
      int realChildPos = Math.max(JrsXmlTools.getNodeRelPos(lastBaseNode, true) + childCorr, 1);
      addDulInsertElement(dulElement, JrsXmlTools.getXpath(lastBaseNode.getParentNode(), true, childCorr), String.valueOf(realChildPos),
          String.valueOf(node.getNodeType()), node.getNodeName(), null);
      String bnXpath = JrsXmlTools.getXpath(lastBaseNode, true, childCorr);
      NodeList chList = node.getChildNodes();
      NamedNodeMap caMap = node.getAttributes();
      int caLen = caMap.getLength();
      for (int ll = 0; ll < caLen; ll++) {
        Node chNode = caMap.item(ll);
        putDulContent(dulElement, chNode, bnXpath, null, node);
      }
      int chLen = chList.getLength();
      int childNo = 1;
      String childNoStr = null;
      for (int ll = 0; ll < chLen; ll++) {
        short chnType = node.getNodeType();
        Node chNode = chList.item(ll);
        if (chnType == Node.ELEMENT_NODE || chnType == Node.TEXT_NODE) {
          childNoStr = String.valueOf(childNo);
          childNo++;
        } else
          childNoStr = null;
        putDulContent(dulElement, chNode, bnXpath, childNoStr, node);
      }
      mParentRelChildCount.put(lastBaseNode.getParentNode(), childCorr + 1);
    }
  }

  /***
   * Generate a DUL delete element for the node
   * 
   * @param dulElement
   *          - the DUL element to append to
   * @param node
   *          - actual node of the reference media
   */
  protected void putDulDelete(Element dulElement, Node node) {
    int childCorr = 0;
    if (mParentRelChildCount.containsKey(node.getParentNode())) {
      childCorr = mParentRelChildCount.get(node.getParentNode()).intValue();
    }
    Element elIns = JrsXmlTools.AddChildElement(dulElement, Text_delete);
    JrsXmlTools.AddChildAttribute(elIns, Text_node, JrsXmlTools.getXpath(node, true));
    mParentRelChildCount.put(node.getParentNode(), childCorr - 1);
  }

  /***
   * Generate a DUL split structure
   * 
   * @param baseNode
   *          - node data of the reference media
   * @param diffNodeDataArr
   *          - node data array of the benchmark media
   * @param dnFrom
   *          - from position in the diffNodeDataArr
   * @param dnTill
   *          - till position in the diffNodeDataArr
   */
  protected void generateSplit(nodeData baseNode, nodeData[] diffNodeDataArr, int dnFrom, int dnTill) {
    Element splitNode = JrsXmlTools.AddChildElement(mDulElement, Text_split);
    Element sourceNode = JrsXmlTools.AddChildElement(splitNode, Text_source);
    putDulDelete(sourceNode, baseNode.mOrigDomNode);
    Element targetNode = JrsXmlTools.AddChildElement(splitNode, Text_target);
    nodeData nData;
    for (int rr = dnFrom; rr <= dnTill; rr++) {
      nData = diffNodeDataArr[rr];
      putDulInsert(targetNode, baseNode.mOrigDomNode, nData.mOrigDomNode);
    }
  }

  /***
   * Generate DUL merger structure
   * 
   * @param baseNodeDataArr
   *          - node data array of the reference media
   * @param diffNodeData
   *          - node data of the benchmark media
   * @param bnFrom
   *          - from position in the baseNodeDataArr
   * @param bnTill
   *          - till position in the baseNodeDataArr
   */
  protected void generateMerge(nodeData[] baseNodeDataArr, nodeData diffNodeData, int bnFrom, int bnTill) {
    Element mergeNode = JrsXmlTools.AddChildElement(mDulElement, Text_merge);
    Element sourceNode = JrsXmlTools.AddChildElement(mergeNode, Text_source);
    nodeData nData;
    for (int rr = bnFrom; rr <= bnTill; rr++) {
      nData = baseNodeDataArr[rr];
      putDulDelete(sourceNode, nData.mOrigDomNode);
    }
    Element targetNode = JrsXmlTools.AddChildElement(mergeNode, Text_target);
    putDulInsert(targetNode, baseNodeDataArr[bnFrom].mOrigDomNode, diffNodeData.mOrigDomNode);
  }

  /***
   * Find the MediaRelIncrTimePoint and MediaIncrDuration nodes of the
   * baseNodeData and update to the values of the diffNodeData
   * 
   * @param baseNodeData
   *          - node data of the reference media
   * @param diffNodeData
   *          - node data of the benchmark media
   * @throws XPathExpressionException
   */
  protected void putDulUpdate(nodeData baseNodeData, nodeData diffNodeData) throws XPathExpressionException {
    String startTimeString = mXpExprTimePoint.evaluate(baseNodeData.mOrigDomNode);
    String durationString = mXpExprDuration.evaluate(baseNodeData.mOrigDomNode);
    XPathExpression xpExprDurationNode = mXpath.compile(mExprDurationNode);
    XPathExpression xpExprTimePointNode = mXpath.compile(mExprTimePointNode);
    Node timePointNode = (Node) xpExprTimePointNode.evaluate(baseNodeData.mOrigDomNode, XPathConstants.NODE);
    Node durationNode = (Node) xpExprDurationNode.evaluate(baseNodeData.mOrigDomNode, XPathConstants.NODE);
    Element updateNode = JrsXmlTools.AddChildElement(mDulElement, Text_update, String.valueOf(diffNodeData.mStartTime));
    String xpath = JrsXmlTools.getXpath(timePointNode, true);
    JrsXmlTools.AddChildAttribute(updateNode, Text_node, xpath);
    JrsXmlTools.AddChildAttribute(updateNode, Text_charpos, "1");
    JrsXmlTools.AddChildAttribute(updateNode, Text_length, String.valueOf(startTimeString.length()));
    updateNode = JrsXmlTools.AddChildElement(mDulElement, Text_update, String.valueOf(diffNodeData.mEndTime - diffNodeData.mStartTime));
    xpath = JrsXmlTools.getXpath(durationNode, true);
    JrsXmlTools.AddChildAttribute(updateNode, Text_node, xpath);
    JrsXmlTools.AddChildAttribute(updateNode, Text_charpos, "1");
    JrsXmlTools.AddChildAttribute(updateNode, Text_length, String.valueOf(durationString.length()));
  }

  /***
   * Check the adjacent diffNodes for split and adjacent baseNodes for merge
   * 
   * @param baseNodeDataArr
   *          - node data array of the reference media
   * @param diffNodeDataArr
   *          - node data array of the benchmark media
   * @param mtBaseTimeUnit
   *          - time unit of the reference media
   * @param mtToDiffTimeUnit
   *          - time unit of the benchmark media
   * @param epsilon
   *          - the allowed deviation of between boundaries (relative value,part
   * @param i
   *          - index of the baseNodeDataArr
   * @param k
   *          - index of the diffNodeDataArr
   * @return - true if split or merge was inserted
   */
  protected boolean checkMergeOrSplit(nodeData[] baseNodeDataArr, nodeData[] diffNodeDataArr, double mtBaseTimeUnit,
      double mtToDiffTimeUnit, double epsilon, int i, int k) {
    boolean retval = false;
    int testRes = 0;
    int dnFrom = k;
    int dnMax = diffNodeDataArr.length - 1;
    testRes = dnFrom > 0 ? testNode(baseNodeDataArr[i], diffNodeDataArr[dnFrom - 1], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon) : 0;
    while (dnFrom > 0 && testRes == 1) {
      dnFrom--;
      testRes = testNode(baseNodeDataArr[i], diffNodeDataArr[dnFrom - 1], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon);
    }
    int dnTill = k;
    if (testRes != 2 && dnTill < dnMax) {
      testRes = testNode(baseNodeDataArr[i], diffNodeDataArr[dnTill + 1], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon);
      while (dnTill < dnMax && testRes == 1) {
        dnTill++;
        testRes = testNode(baseNodeDataArr[i], diffNodeDataArr[dnTill + 1], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon);
      }
    }
    if (testRes != 2) {
      if (dnTill > dnFrom) {
        generateSplit(baseNodeDataArr[i], diffNodeDataArr, dnFrom, dnTill);
        retval = true;
      }
    } else {
      int bnFrom = i;
      int bnMax = baseNodeDataArr.length - 1;
      testRes = bnFrom > 0 ? testNode(baseNodeDataArr[bnFrom - 1], diffNodeDataArr[k], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon) : 0;
      while (bnFrom > 0 && testRes == 1) {
        bnFrom--;
        testRes = bnFrom > 0 ? testNode(baseNodeDataArr[bnFrom - 1], diffNodeDataArr[k], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon) : 0;
      }
      int bnTill = i;
      if (testRes != 2 && bnTill < bnMax) {
        testRes = testNode(baseNodeDataArr[bnTill + 1], diffNodeDataArr[k], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon);
        while (bnTill < bnMax && testRes == 1) {
          bnTill++;
          testRes = bnTill < bnMax ? testNode(baseNodeDataArr[bnTill + 1], diffNodeDataArr[k], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon)
              : 0;
        }
      }
      if (testRes != 2) {
        if (bnTill > bnFrom) {
          generateMerge(baseNodeDataArr, diffNodeDataArr[k], bnFrom, bnTill);
          retval = true;
        }
      }
    }
    return (retval);
  }

  /***
   * Generate the result DOM
   * 
   * @param cVal
   *          - the LCS Length matrix
   * @param mtBaseTimeUnit
   *          - time unit of the reference media
   * @param mtToDiffTimeUnit
   *          - time unit of the benchmark media
   * @param epsilon
   *          - the allowed deviation of between boundaries (relative value,part
   *          of the segment length)
   * @param baseNodeDataArr
   *          - data vector with the values of the reference media
   * @param diffNodeDataArr
   *          - data vector with the values of the benchmark media
   * @param i
   *          - index of the baseNodeDataArr
   * @param k
   *          - index of the diffNodeDataArr
   * @throws XPathExpressionException
   */
  protected void printDiff(int[][] cVal, double mtBaseTimeUnit, double mtToDiffTimeUnit, double epsilon, nodeData[] baseNodeDataArr,
      nodeData[] diffNodeDataArr, int i, int k) throws XPathExpressionException {
    Vector<StringBuffer> retval = null;
    if (i >= 0 && k >= 0 && matchNodes(baseNodeDataArr[i], diffNodeDataArr[k], mtBaseTimeUnit, mtToDiffTimeUnit, epsilon)) {
      printDiff(cVal, mtBaseTimeUnit, mtToDiffTimeUnit, epsilon, baseNodeDataArr, diffNodeDataArr, i - 1, k - 1);
      mLastBaseNode = baseNodeDataArr[i].mOrigDomNode;
      if (mVerbose)
        System.out.println("  [" + baseNodeDataArr[i].mStartTime + "-" + baseNodeDataArr[i].mEndTime + "]" + " rPos:"
            + JrsXmlTools.getNodeRelPos(baseNodeDataArr[i].mOrigDomNode, false));
      if (!checkMergeOrSplit(baseNodeDataArr, diffNodeDataArr, mtBaseTimeUnit, mtToDiffTimeUnit, epsilon, i, k))
        putDulUpdate(baseNodeDataArr[i], diffNodeDataArr[k]);
    } else if (k >= 0 && ((i == -1) || ((k > 0 ? cVal[i][k - 1] : 0) >= (i > 0 ? cVal[i - 1][k] : 0)))) {
      int ckm1 = (k > 0 ? cVal[i][k - 1] : 0);
      int cim1 = (i > 0 ? cVal[i - 1][k] : 0);
      printDiff(cVal, mtBaseTimeUnit, mtToDiffTimeUnit, epsilon, baseNodeDataArr, diffNodeDataArr, i, k - 1);
      if (mLastBaseNode == null)
        mLastBaseNode = baseNodeDataArr[0].mOrigDomNode;
      if (mVerbose)
        System.out.println("+  [" + diffNodeDataArr[k].mStartTime + "-" + diffNodeDataArr[k].mEndTime + "]" + " ckm1:" + ckm1 + " cim1:"
            + cim1);
      putDulInsert(mDulElement, mLastBaseNode, diffNodeDataArr[k].mOrigDomNode);
    } else if (i >= 0 && ((k == -1) || ((k > 0 ? cVal[i][k - 1] : 0) < (i > 0 ? cVal[i - 1][k] : 0)))) {
      printDiff(cVal, mtBaseTimeUnit, mtToDiffTimeUnit, epsilon, baseNodeDataArr, diffNodeDataArr, i - 1, k);
      if (mVerbose)
        System.out.println("- [" + baseNodeDataArr[i].mStartTime + "-" + baseNodeDataArr[i].mEndTime + "]" + " rPos:"
            + JrsXmlTools.getNodeRelPos(baseNodeDataArr[i].mOrigDomNode, false));
      putDulDelete(mDulElement, baseNodeDataArr[i].mOrigDomNode);
    } else if (mVerbose)
      System.out.println();
    // return(retval);
  }

  /***
   * Parse the command line arguments
   * 
   * @param args
   *          - string array from the command line
   */
  protected static void processParams(String[] args) {
    Iterator<String> aIter = Arrays.asList(args).iterator();
    String astr;
    try {
      while (aIter.hasNext()) {
        astr = aIter.next();
        if (astr.charAt(0) == '-') {
          if (aIter.hasNext() && astr.compareTo("-epsilon") == 0) {
            astr = aIter.next();
            mEpsilon = Double.parseDouble(astr);
          } else if (aIter.hasNext() && astr.compareTo("-compGE") == 0) {
            astr = aIter.next();
            mCompareGE = Integer.parseInt(astr) == 1;
          } else if (aIter.hasNext() && astr.compareTo("-O") == 0) {
            astr = aIter.next();
            mOutputFileName = astr;
          } else if (astr.compareTo("-v") == 0) {
            mVerbose = true;
          }
        } else if (mParamBaseFileName == null) {
          mParamBaseFileName = astr;
        } else if (mParamToDiffFileName == null) {
          mParamToDiffFileName = new Vector<String>();
          mParamToDiffFileName.add(astr);
        } else {
          mParamToDiffFileName.add(astr);
        }
      }
    } catch (NumberFormatException ex) {
      ex.printStackTrace();
    }
    if (mOutputFileName == null || mParamToDiffFileName == null) {
      System.out.println("Usage: Mpeg7Diff [-epsilon epsilonValue] [-compGE (0/1)] [-O OutputFileName] BaseFileName DiffFileName");
      System.exit(1);
    }
  }

  /***
   * Do the actual processing: create the LCS Length matrix, do backtracking and
   * crate the DUL DOM.
   * 
   * @throws SAXException
   * @throws IOException
   */
  protected void doProcess() throws SAXException, IOException {
    nodeData[] baseNodeData = null;
    nodeData[] diffNodeData = null;
    Document xmlDocument = null;
    Document xmlDocumentToDiff = null;
    xmlDocument = mDocBuilder.parse(mBaseFileName);
    xmlDocumentToDiff = mDocBuilder.parse(mToDiffFileName);
    if (xmlDocument != null && xmlDocumentToDiff != null) {
      int refIdx = 0;
      try {
        // read a nodelist using xpath
        double mtBaseTimeUnit = getBaseTimeUnit(mXpath.compile(mXpathMediaTimeUnit), xmlDocument);
        double mtToDiffTimeUnit = getBaseTimeUnit(mXpath.compile(mXpathMediaTimeUnit), xmlDocumentToDiff);
        NodeList nodeListBase = (NodeList) mXpath.compile(mXPathAudiosVisual).evaluate(xmlDocument, XPathConstants.NODESET);
        NodeList nodeListDiff = (NodeList) mXpath.compile(mXPathAudiosVisual).evaluate(xmlDocumentToDiff, XPathConstants.NODESET);
        NodeList nodeListBaseId = (NodeList) mXpath.compile(mXPathAudiosVisualId).evaluate(xmlDocument, XPathConstants.NODESET);
        NodeList nodeListBaseTime = (NodeList) mXpath.compile(mXPathAudiosVisualTime).evaluate(xmlDocument, XPathConstants.NODESET);
        NodeList nodeListBaseDuration = (NodeList) mXpath.compile(mXPathAudiosVisualDuration).evaluate(xmlDocument, XPathConstants.NODESET);
        NodeList nodeListDiffId = (NodeList) mXpath.compile(mXPathAudiosVisualId).evaluate(xmlDocumentToDiff, XPathConstants.NODESET);
        NodeList nodeListDiffTime = (NodeList) mXpath.compile(mXPathAudiosVisualTime).evaluate(xmlDocumentToDiff, XPathConstants.NODESET);
        NodeList nodeListDiffDuration = (NodeList) mXpath.compile(mXPathAudiosVisualDuration).evaluate(xmlDocumentToDiff,
            XPathConstants.NODESET);
        int ncBase = nodeListBaseId.getLength();
        int ncDiff = nodeListDiffId.getLength();
        baseNodeData = new nodeData[ncBase];
        diffNodeData = new nodeData[ncDiff];
        long stTime;
        long stDuration;
        for (int j = 0; j < ncBase; j++) {
          stTime = Long.parseLong(nodeListBaseTime.item(j).getNodeValue());
          stDuration = Long.parseLong(nodeListBaseDuration.item(j).getNodeValue());
          baseNodeData[j] = new nodeData(nodeListBaseId.item(j).getNodeValue(), stTime, stTime + stDuration, nodeListBase.item(j));
        }
        for (int k = 0; k < ncDiff; k++) {
          stTime = Long.parseLong(nodeListDiffTime.item(k).getNodeValue());
          stDuration = Long.parseLong(nodeListDiffDuration.item(k).getNodeValue());
          diffNodeData[k] = new nodeData(nodeListDiffId.item(k).getNodeValue(), stTime, stTime + stDuration, nodeListDiff.item(k));
        }
        int cVal[][] = lcsLength(mtBaseTimeUnit, mtToDiffTimeUnit, mEpsilon, baseNodeData, diffNodeData);
        mBckRes = new HashSet[cVal.length][cVal[0].length];
        for (HashSet<String>[] hs : mBckRes) {
          for (HashSet<String> hsStr : hs) {
            hsStr = null;
          }
        }
        HashSet<String> bResX = backTrackAll(cVal, mtBaseTimeUnit, mtToDiffTimeUnit, mEpsilon, baseNodeData, diffNodeData,
            (int) (ncBase) - 1, (int) (ncDiff) - 1, true, mCompareGE); // 12,
                                                                       // 12);
        /*
         * Vector<StringBuffer> bRes = backTrackAll(cVal, mtBaseTimeUnit,
         * mtToDiffTimeUnit, mEpsilon, baseNodeData, diffNodeData, ncBase - 1,
         * ncDiff - 1, false, mCompareGE); // 12,
         */
        if (mVerbose)
          System.out.print("    ;");
        for (nodeData nData : diffNodeData) {
          if (mVerbose)
            System.out.print(+nData.mStartTime + "-" + nData.mEndTime + ";");
        }
        if (mVerbose)
          System.out.println();
        for (int i = 0; i < cVal.length; i++) {
          for (int k = 0; k < cVal[i].length; k++) {
            if (k == 0)
              if (mVerbose)
                System.out.print(+baseNodeData[i].mStartTime + "-" + baseNodeData[i].mEndTime + ";");
            String bp = "";
            if (mIsMatch[i][k] == 1)
              bp = "*";
            else if (mWasChecked[i][k] == 1)
              bp = "#";
            if (mVerbose)
              System.out.print(bp + cVal[i][k] + ";");
          }
          if (mVerbose)
            System.out.println();
        }

        TreeSet<String> resSet = new TreeSet<String>();
        for (String pstr : bResX) {
          resSet.add(pstr.toString());
        }
        if (mVerbose)
          System.out.println("--------------------X Values-------------------------------------");
        for (String pstr : resSet) {
          if (mVerbose)
            System.out.println(pstr);
        }

        resSet = new TreeSet<String>();
        /*
         * for (StringBuffer pstr : bRes) { resSet.add(pstr.toString()); }
         * System.out.println(
         * "--------------------Y Values-------------------------------------"
         * ); for (String pstr : resSet) { System.out.println(pstr); }
         */
        /*
         * System.out.println(
         * "--------------------End Hash-------------------------------------"
         * ); for (StringBuffer pstr : bRes) {
         * System.out.println(pstr.toString()); }
         */
        if (mVerbose)
          System.out.println("--------------------Print Diff-------------------------------------");
        mLastBaseNode = null;
        printDiff(cVal, mtBaseTimeUnit, mtToDiffTimeUnit, mEpsilon, baseNodeData, diffNodeData, ncBase - 1, ncDiff - 1);
      } catch (XPathExpressionException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  /***
   * Create the DUL DOM for 2 files
   * 
   * @param referenceFile
   *          - reference file name
   * @param benchmarkingFile
   *          - benchmark file name
   * @return - the result DOM
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws XPathExpressionException
   */
  public static Document mp7Diff(String referenceFile, String benchmarkingFile) throws ParserConfigurationException, SAXException,
      IOException, XPathExpressionException {
    Vector<String> bmFiles = new Vector<String>();
    bmFiles.add(benchmarkingFile);
    Document xmlDocument = mp7Diff(referenceFile, bmFiles);
    return (xmlDocument);
  }

  /***
   * Create the DUL DOM for one reference file and benachark file set
   * 
   * @param referenceFile
   *          - reference file name
   * @param benchmarkingFiles
   *          - vector of benchmark file names
   * @return - the result DOM
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws XPathExpressionException
   */
  public static Document mp7Diff(String referenceFile, Vector<String> benchmarkingFiles) throws ParserConfigurationException, SAXException,
      IOException, XPathExpressionException {
    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = null;
    builder = builderFactory.newDocumentBuilder();
    Document xmlDocument = builder.newDocument();
    mXpExprTimePoint = mXpath.compile(mExprTimePoint);
    mXpExprDuration = mXpath.compile(mExprDuration);
    mDulSetElement = JrsXmlTools.AddChildElement(xmlDocument, "DULSet");
    for (String bmFile : benchmarkingFiles) {
      Element dulElement = JrsXmlTools.AddChildElement(mDulSetElement, "DUL");
      Mp7Diff mp7Diff = new Mp7Diff(referenceFile, bmFile, dulElement, builder);
      mp7Diff.doProcess();
    }
    return (xmlDocument);
  }

  /**
   * @param args
   * @throws TransformerException
   * @throws TransformerFactoryConfigurationError
   */
  public static void main(String[] args) throws TransformerFactoryConfigurationError, TransformerException {
    processParams(args);
    try {
      Document xmlDocument = mp7Diff(mParamBaseFileName, mParamToDiffFileName);
      JrsXmlTools.SerializeDom(xmlDocument, mOutputFileName);
    } catch (ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (SAXException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (XPathExpressionException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
