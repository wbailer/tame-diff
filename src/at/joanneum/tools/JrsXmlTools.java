/*

  TAME-Diff 
  
  Developed with the TOSCA-MP project (http://www.tosca-mp.eu)
    
  2014 JOANNEUM RESEARCH Forschungsgesellschaft mbH, Graz, Austria
  http://www.joanneum.at/en/digital/
  
  
  ----------------------------------------------------------------------------
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


 */

package at.joanneum.tools;

import java.io.File;
import java.io.StringWriter;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * 
 */

/**
 * @author hoa
 * 
 */
public class JrsXmlTools {

  static public org.w3c.dom.Element AddChildElement(org.w3c.dom.Node el, String Name) {
    short nType = el.getNodeType();
    Document doc = null;
    if (nType == Node.DOCUMENT_NODE) {
      doc = (Document) el;
    } else {
      doc = el.getOwnerDocument();
    }
    org.w3c.dom.Element item = doc.createElementNS(doc.getNamespaceURI(), Name);
    el.appendChild(item);
    item.normalize();
    return (item);
  }

  static public org.w3c.dom.Element AddChildElement(org.w3c.dom.Node el, String Name, String Text) {
    Document doc = el.getOwnerDocument();
    // ClearXpathCache(doc);
    org.w3c.dom.Element item = doc.createElementNS(el.getNamespaceURI(), Name);
    el.appendChild(item);
    item.normalize();
    if (Text != null)
      item.appendChild(doc.createTextNode(Text));
    return (item);
  }

  static public org.w3c.dom.Node AddChildText(org.w3c.dom.Node el, String Text) {
    Node retval = null;
    if (Text != null) {
      Document doc = el.getOwnerDocument();
      // ClearXpathCache(doc);
      retval = el.appendChild(doc.createTextNode(Text));
    }
    return (retval);
  }

  static public void AddChildAttribute(org.w3c.dom.Element el, String attrName, String Text) {
    if (attrName != null && Text != null) {
      // ClearXpathCache(doc);
      el.setAttribute(attrName, Text);
    }
  }

  static public org.w3c.dom.Element InsertChildElement(org.w3c.dom.Element el, String Name) {
    return (InsertChildElement(el, Name, null, null));
  }

  static public org.w3c.dom.Element InsertChildElement(org.w3c.dom.Element el, String Name, String Text, org.w3c.dom.Node beforeElement) {
    Document doc = el.getOwnerDocument();
    // ClearXpathCache(doc);
    org.w3c.dom.Element item = doc.createElement(Name);
    if (beforeElement == null)
      beforeElement = el.getFirstChild();
    el.insertBefore(item, beforeElement);
    item.normalize();
    if (Text != null)
      item.appendChild(doc.createTextNode(Text));
    return (item);
  }

  static public String SerializeDom(Node document) throws TransformerFactoryConfigurationError, TransformerException {
    String retval = null;
    StringWriter strWriter = new StringWriter();
    Transformer transformer = null;
    transformer = TransformerFactory.newInstance().newTransformer();
    Result output = new StreamResult(strWriter);
    Source input = new DOMSource(document);
    transformer.transform(input, output);
    retval = strWriter.toString();
    return (retval);
  }

  static public boolean SerializeDom(Node document, String fileName) throws TransformerFactoryConfigurationError, TransformerException {
    boolean retval = true;
    Transformer transformer = null;
    transformer = TransformerFactory.newInstance().newTransformer();
    Result output = new StreamResult(new File(fileName));
    Source input = new DOMSource(document);
    transformer.transform(input, output);
    return (retval);
  }

  static public int getNodeRelPos(Node element, boolean byNodeName) {
    int retval = 0;
    short nType = element.getNodeType();
    String nodeName = element.getNodeName();
    if (nType == Node.ELEMENT_NODE) {
      int count = 1;
      Node ps = element.getPreviousSibling();
      while (ps != null) {
        if (ps.getNodeType() == Node.ELEMENT_NODE) {
          if (byNodeName) {
            if (nodeName.compareTo(ps.getNodeName()) == 0)
              count++;
          } else
            count++;
        }
        ps = ps.getPreviousSibling();
      }
      retval = count;
    }
    return (retval);
  }

  static public String getRelXpath(Node element, Node baseElement, boolean byNodeName) {
    String retval = "";
    String nodeName = element.getNodeName();
    if (!byNodeName)
      nodeName = "node()";
    if (element != baseElement) {
      short nType = element.getNodeType();
      if (nType == Node.ELEMENT_NODE) {
        int count = getNodeRelPos(element, byNodeName);
        retval = getRelXpath(element.getParentNode(), baseElement, byNodeName);
        if (retval.length() > 1)
          retval += "/" + nodeName + "[" + String.valueOf(count) + "]";
        else
          retval += nodeName + "[" + String.valueOf(count) + "]";
      }
    }
    return (retval);
  }

  static public String getXpath(Node element, boolean byNodeName, int childCorr) {
    String retval = null;
    short nType = element.getNodeType();
    String nodeName = element.getNodeName();
    if (!byNodeName)
      nodeName = "node()";
    if (nType == Node.ELEMENT_NODE) {
      int count = getNodeRelPos(element, byNodeName);
      retval = getXpath(element.getParentNode(), byNodeName);
      count = count + childCorr;
      count = Math.max(count, 1);
      if (retval.length() > 1)
        retval += "/" + nodeName + "[" + String.valueOf(count) + "]";
      else
        retval += "" + nodeName + "[" + String.valueOf(count) + "]";
    } else if (nType == Node.DOCUMENT_NODE) {
      retval = "/";
    }
    return (retval);
  }

  static public String getXpath(Node element, boolean byNodeName) {
    return (getXpath(element, byNodeName, 0));
  }

}
